package eventmanagers

import (
	"time"
)

// Event should represent
type Event struct {
	id          string
	source      string
	description string
	eventDates  []EventTimeSpan
}

// EventTimeSpan should represent
type EventTimeSpan struct {
	beginDate time.Time
	endDate   time.Time
}

// EventAPI should represent
type EventAPI interface {
	getEvents() []Event
}
