package eventmanagers

import "github.com/Guitarbum722/meetup-client"

// Meetup should
type Meetup struct {
	client *meetup.ClientOpts
}

// NewAPI is responsible for creating a new Meetup
// structure
func NewAPI(apiToken string) *Meetup {
	return &Meetup{
		client: &meetup.ClientOpts{
			APIKey: apiToken,
		},
	}
}

func getEvents() []Event {
	return nil
}
